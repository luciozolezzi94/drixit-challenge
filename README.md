# drixit-challenge #

## Introduction ##

DevOps interview challenge for Drixit

## Requirements ##

* Docker images
    * drixit/challenge-web:latest
    * drixit/challenge-api:latest
    * mongo-db:4.2
* docker-compose
* kompose
* kubectl
* minikube

## Proposed architecture ##

### Networking ###

![Esquema containers](docs/images/architecture.png)

For networking, a VPC layered 3x3 is proposed, layed out in the following matter:

* 3 public subnet (distributed in 3 AZ - (NAT GWs and anything that needs to be in public subnet)
* 3 private subnet for data (distributed in 3 AZs) - Nodes for `drixit/challenge-web:latest` and `drixit/challenge-api:latest`
* 3 private subnet for apps (distributed in 3 AZs) - Nodes for `mongo:4.2`

### Security ###

For security point of view, is recommended always to follow the least privilege methodology, where only minimal access is granted to a resource that needs it. That applies for IAM and Networking as well.

## Deployment options ##

For deployment of the project it is proposed to an IaaC tool depending on the scenario that you are facing. Usually, I recommend

* Hashicorp terrafor (multi cloud tool)
* sceptre - best tool for managing AWS CloudFormation templates

## Running docker image of the project ##

For running the docker containers of the project locally to test them, run

`docker-compose up -d`

After testing them, you can transform your docker-compose files into k8s files with the tool `kompose` as shown

`kompose convert --volumes hostPath`

## Proposition for no downtime deployment ##

The best method for a deployment with no downtime is to use a rolling-update method, which consist in updating nodes in small batches while the older ones are still available that way you always have nodes available

